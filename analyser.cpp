#include <cstdio>
#include <cstdlib>

#include <cmath>
#include <string>

#include "Analysis.hpp"
#include "Mesh.hpp"

#include <limits.h>

#include <assert.h>

#include <unistd.h>
#include <getopt.h>

static struct option longopts[] = {
	{"meshfile", 1, NULL, 'f'},
	{NULL, 0, NULL, 0}
};

void
usage(void)
{
	fprintf(stderr, "Usage: analyser\n");
}

/*
input format:
dimensions
n vect[0:n-1] \
m vect[0:m-1]   - x dimensions
k vect[0:k-1] /
*/

int max_sn = -1;

typedef struct {
	int	  dimensions;
	int	 *counts;
	int	 *p;
	int	**vectors;
	int	**functions;
	Element	 *elements;
} spline_space;

typedef struct {
	int	 *verts;
	int	 *cols;
	int	 *crs;
} graph;

int
supernode_nr(int *vec, spline_space *sp)
{
	int id;
	int tmp;
	int i, j;

	id = 0;
	j = 1;
	for (i = sp->dimensions - 1; i >= 0; --i) {
		id += vec[i] * j;
		j *= sp->functions[i][sp->vectors[i][sp->counts[i] - 1] - 1] + sp->p[i] + 1;
	}

	return (id);
}

int
enumerate_supernodes(Element *e, int dim, int *el, int *v, spline_space *sp)
{
	int i;
	int sn;

	for (i = 0; i < sp->p[dim]+1; ++i) {
		v[dim] = sp->functions[dim][el[dim]] + i;
		if (dim != sp->dimensions - 1)
			sn = enumerate_supernodes(e, dim + 1, el, v, sp);
		else {
			sn = supernode_nr(v, sp);
			e->supernodes.push_back(sn);
		}

		if (sn > max_sn)
			max_sn = sn;
	}
	return sn;
}

Node *
builder(int node_id, spline_space *sp, int *p1, int *p2)
{
	int *pmiddle;
	int dim_max;
	int max;
	int i;
	Node *n;

	for (i = 0; i < sp->dimensions; ++i)
		assert(p2[i] - p1[i] >= 0);

	dim_max = -1;
	max = -1;
	pmiddle = (int *) calloc(sp->dimensions, sizeof(int));

	for (i = 0; i < sp->dimensions; ++i) {
		if (p2[i] - p1[i] > max) {
			dim_max = i;
			max = p2[i] - p1[i];
		}
	}

	n = new Node();
	n->setId(node_id);

	if (max >= 1) {
		for (i = 0; i < sp->dimensions; ++i) {
			if (i != dim_max)
				pmiddle[i] = p2[i];
			else
				pmiddle[i] = (p2[i] + p1[i]) / 2;
		}
		n->setLeft(builder(2 * node_id + 1, sp, p1, pmiddle));
		for (i = 0; i < sp->dimensions; ++i) {
			if (i != dim_max)
				pmiddle[i] = p1[i];
			else
				pmiddle[i] = (p2[i] + p1[i]) / 2 + 1;
		}
		n->setRight(builder(2 * node_id + 2, sp, pmiddle, p2));
		for (Element *e : n->getLeft()->getElements())
			n->addElement(e);
		for (Element *e : n->getRight()->getElements())
			n->addElement(e);
	} else {
		Element *e = new Element();
		int *vec = (int *)calloc(sp->dimensions, sizeof(int));
		enumerate_supernodes(e, 0, p1, vec, sp);
		free(vec);
		n->addElement(e);
	}

	free(pmiddle);
	return (n);
}

Mesh *
build_mesh(spline_space *sp)
{
	int i;
	int j;
	int k;
	int *p1;
	int *p2;
	Node *root;
	Mesh *m;

	p1 = (int *)calloc(sp->dimensions, sizeof(int));
	p2 = (int *)calloc(sp->dimensions, sizeof(int));

	sp->functions = (int**) calloc(sp->dimensions, sizeof(int*));

	for (i = 0; i < sp->dimensions; ++i) {
		sp->functions[i] = (int*)calloc(sp->vectors[i][sp->counts[i]-1], sizeof(int));
		assert(sp->functions[i] != NULL);
		j = sp->p[i];
		k = 0;
		while (k < sp->vectors[i][sp->counts[i] - 1] && j < sp->counts[i]) {
			if (sp->vectors[i][j] < sp->vectors[i][j + 1]) {
				sp->functions[i][k] = j - sp->p[i];
				++k;
			}
			++j;
		}
		p1[i] = 0;
		p2[i] = sp->vectors[i][sp->counts[i] - 1] - 1;
	}

	root = builder(0, sp, p1, p2);
	m = new Mesh();
	m->setRootNode(root);

	for (i = 0; i <= max_sn; ++i)
		m->addSupernode(1);

	m->setNodeSupernodes(root);
	free(p1);
	free(p2);
	return m;
}

int main(int argc, char ** argv)
{
	Mesh *mesh;
	char *meshfile;
	spline_space *sp;
	int i, j, ch;

	meshfile = NULL;
	while ((ch = getopt_long(argc, argv, "f:", longopts, NULL)) != -1) {
		switch (ch) {
		case 'f':
			meshfile = optarg;
			break;
		default:
			usage();
			return (1);
		}
	}

	mesh = Mesh::loadFromFile(meshfile);
	if (mesh == NULL) {
		fprintf(stderr, "Could not load mesh file from: %s\n", meshfile);
		return (1);
	}
	Analysis::doAnalise(mesh);
	Analysis::estimateElimCosts(mesh->getRootNode());
	for (i = 0; i < mesh->getSupernodes(); i++)
		printf("%lu = %lu\n", i, mesh->flops[i]);
	delete mesh;
	return (0);
}
