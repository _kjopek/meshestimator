#include "Mesh.hpp"
#include <set>
#include <map>
#include <tuple>
#include <algorithm>

void Mesh::addNode(Node *n)
{
	this->nodes.push_back(n);
}

void Mesh::addElement(Element *e)
{
	this->elements.push_back(e);
}

void  Mesh::setSupernodes(uint64_t supernodes)
{
	this->supernode_count = supernodes;
	this->supernodes.resize(supernodes);
	this->flops.resize(supernodes);
}

uint64_t Mesh::getSupernodes()
{
	return this->supernode_count;
}

void Mesh::addSupernode(uint64_t supernode)
{
	this->supernodes.push_back(supernode);
	this->flops.push_back(0);
}

void Mesh::setNodeSupernodes(Node *n)
{
	n->meshSupernodes = &this->supernodes;
	if (n->getLeft() && n->getRight()) {
		setNodeSupernodes(n->getLeft());
		setNodeSupernodes(n->getRight());
	}
}

uint64_t Mesh::getTotalDofs()
{
	uint64_t total;

	total = 0;
	for (uint64_t i = 0; i < this->supernodes.size(); ++i)
		total += this->supernodes[i];
	return total;
}

std::vector<Element *> &Mesh::getElements()
{
	 return this->elements;
}

Node *Mesh::getRootNode()
{
	 return this->root;
}

Mesh *Mesh::loadFromFile(const char *filename)
{
	FILE *fp;
	Mesh *mesh;
	fp = fopen(filename, "r");
	if (fp == NULL) {
		perror("fopen");
		return NULL;
	}
	uint64_t supernodes = 0;
	uint64_t nodes = 0;
	uint64_t elements;

	fscanf(fp, "%lu", &supernodes);

	mesh = new Mesh();
	mesh->setSupernodes(supernodes);
	for (uint64_t i = 0; i < supernodes; ++i) {
		uint64_t tmp;
		uint64_t id;
		fscanf(fp, "%lu %lu", &id, &tmp);
		mesh->supernodes[i] = tmp;
		mesh->flops[i] = 0;
	}

	fscanf(fp, "%lu", &elements);

	std::map<std::tuple<uint64_t, uint64_t>, Element*> elementsMap;
	std::vector<Node *> nodesVector;

	for (uint64_t i=0; i<elements; ++i) {
		uint64_t k, l;
		fscanf(fp, "%lu %lu", &k, &l);
		Element *e = new Element();
		e->k = k;
		e->l = l;
		std::tuple<uint64_t, uint64_t> t(k,l);
		mesh->addElement(e);
		elementsMap[t] = e;
		fscanf(fp, "%lu", &k);
		for (uint64_t j=0; j<k; ++j) {
			fscanf(fp, "%lu", &l);
			e->supernodes.push_back(l-1);
		}
	}

	fscanf(fp, "%lu", &nodes);
	nodesVector.resize(nodes);

	for (uint64_t i=0; i<nodes; ++i) {
		uint64_t node_id;
		uint64_t nr_elems;
		fscanf(fp, "%lu %lu", &node_id, &nr_elems);
		Node *n = new Node(&mesh->supernodes, &mesh->flops, node_id);
		nodesVector[node_id-1] = n;
		for (uint64_t q=0; q<nr_elems; ++q) {
			uint64_t k, l;
			fscanf(fp, "%lu %lu", &k, &l);
            Element *e = elementsMap[std::tuple<uint64_t,uint64_t>(k, l)];
            if (e == NULL) {
                    fprintf(stderr, "Node %ld points to non-existing element.\n",
                        i);
                    exit(1);
            }
			n->addElement(e);
		}
		if (nr_elems > 1) {
			uint64_t leftSon, rightSon;
			fscanf(fp, "%lu %lu", &leftSon, &rightSon);
			n->n_left = leftSon;
			n->n_right = rightSon;
		} else {
			n->n_left = n->n_right = -1;
		}
		if (i == 0) {
			mesh->root = n;
		}
	}

	// all nodes read? built the Tree!

	for (uint64_t i=0; i<nodes; ++i) {
		int64_t left, right;

		left = nodesVector[i]->n_left ;
		right = nodesVector[i]->n_right ;
#ifdef DEBUG
		fprintf(stderr, "Node: %lu l: %ld r: %ld\n", nodesVector[i]->getId(), left, right);
#endif
		if (left != -1) {
			nodesVector[i]->setLeft(nodesVector[left-1]);
			nodesVector[left-1]->setParent(nodesVector[i]);
		}
		if (right != -1) {
			nodesVector[i]->setRight(nodesVector[right-1]);
			nodesVector[right-1]->setParent(nodesVector[i]);
		}
		mesh->addNode(nodesVector[i]);
	}

	fclose(fp);
	return mesh;
}


void Mesh::setRootNode(Node* root)
{
	this->root = root;
}
