#ifndef NODE_HPP
#define NODE_HPP

#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>

#include "Element.hpp"

#include <set>

class Mesh;

class Node {
	private:
		int node = -1;
		Node *left = NULL;
		Node *right = NULL;
		Node *parent = NULL;
		std::vector<Element *> mergedElements;
		std::string production;
		std::vector<uint64_t> supernodes;
		std::vector<uint64_t> offsets;
		uint64_t supernodesToElim;

	public:
		int n_left = -1;
		int n_right = -1;
		std::vector<uint64_t> *meshSupernodes;
		std::vector<uint64_t> *meshFlops;

		int l=0;
		int r=0;

		std::vector<uint64_t> leftPlaces;
		std::vector<uint64_t> rightPlaces;

		Node(): node(0)  {}

		Node(std::vector<uint64_t> *meshSupernodes, std::vector<uint64_t> *meshFlops, int num):
		meshSupernodes(meshSupernodes), meshFlops(meshFlops), node(num) {}
		void setLeft(Node *left);
		void setRight(Node *right);
		void setParent(Node *parent);

		Node *getLeft() const;
		Node *getRight() const;
		Node *getParent() const;

		void addElement (Element *e);
		std::vector<Element *> &getElements();
		void clearElements();

		void addSupernode(uint64_t supernode);
		std::vector<uint64_t> &getSupernodes();
		void clearSupernodes();
		void computeOffsets();

		int getId() const;
		void setId(int id);
		void setSupernodesToElim(uint64_t dofs);
		uint64_t getSupernodesToElim() const;

		unsigned long getSizeInMemory(bool recursive = true);
		unsigned long getFLOPs(unsigned int a, unsigned int b);
		unsigned long getMemoryRearrangements();

		/*DEBUG*/

		int treeSize();

		/*END OF DEBUG*/
};

#endif // NODE_HPP
