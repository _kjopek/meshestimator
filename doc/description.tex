\documentclass[10pt,a4paper,titlepage, margin=1cm]{article}

\usepackage{url}
\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage{listings}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{palatino}

\lstset{
    language={[95]Fortran},
    basicstyle=\ttfamily
}


\begin{document}
\title{FLOPs estimator internals}
\author{Konrad Jopek}
\date{}
\maketitle
\begin{section}{Introduction}
    In this document I included detailed description of flops estimation method
    from a perspective of single node of partitioning tree. Discussed procedure
    should be applied to all of partitioning tree nodes in order to deliver
    estimation of total cost of sparse matrix factorization. Described method is
    geometry-independent as it relies only on graph analysis and may be applied
    for both FEM and IGA methods.

    While isogeometric analysis assumes that for any supernode only one degree
    of freedom exists, the finite element method may change the number of
    degrees of freedom associated with single supernode. Flops estimator should
    be written in general way and must handle correctly both situations. Meeting
    that condition requires maintaining additional array which contains number
    of degrees of freedom associated with supernode.

    Detailed description of input format that delivers list of supernodes with
    DOFs arity, list of mesh elements and partitioning tree is out of scope of
    this document and will be described separately.
\end{section}
\begin{section}{Equations for computing number of FLOPs per degree of freedom}
    Multi-frontal solver driven by partitioning tree maintains matrix $M$
    associated with every node of the tree. Matrix represents coefficients for
    degrees of freedom (DOFs) that are divided into two main parts:
    \begin{enumerate}
        \item DOFs that will be fully eliminated in given tree node.
        \item DOFs being a part of Schur complement and will be merged to matrix
              associated to parent of a given tree node.
    \end{enumerate}
    The arity of the above groups will be denoted as $a$ and $b$ for number
    of DOFs to be eliminated and size of Schur complement respectively.

    Therefore, content of the matrix $M$ can be splitted into four main
    submatrices as it was illustrated by equation (\ref{eq:matrix_blocks}):
    \begin{equation}
        \label{eq:matrix_blocks}
        M_{n \times n}= \left[
           \begin{array}{c c}
              A_{a \times a} & B_{a \times b} \\
              C_{b \times a} & D_{b \times b} \\
           \end{array}
        \right]
    \end{equation}
    Where: $n = a + b$.

    In every node of the tree solver computes Schur complement. Accounting above
    assumptions the Schur complement in our current notation can be expressed
    using equation (\ref{eq:schur_complement}):
    \begin{equation}
        \label{eq:schur_complement}
        D = D - CA^{-1}B
    \end{equation}
    Above we have one LU or Cholesky factorization, backward and forward
    substitution, one matrix by matrix multiplication $C \times (A^{-1}B)$ and
    one substraction. The costs for all of these operations will be described in
    the following subsections.

\subsection{Factorization}
    Factorization is performed only on matrix $A_{a \times a}$. Exact cost of LU
    factorization is given by equation (\ref{eq:fact_lu_cost}):
    \begin{equation}
    \label{eq:fact_lu_cost}
        f_{\mathrm{lu}}(n) = \frac{2}{3}n^3 - \frac{1}{2}n^2 + \frac{5}{6}n
    \end{equation}
    In case of symmetric, positively-definite matrix, we can use Cholesky
    decomposition which cost is given by the equation
    (\ref{eq:fact_cholesky_cost}):
    \begin{equation}
    \label{eq:fact_cholesky_cost}
        f_{\mathrm{chol}}(n) = \frac{1}{3}n^3 + \frac{1}{2}n^2 + \frac{1}{6}n
    \end{equation}
    In case of LU factorization (typical for nonsymmetric or not
    positively-definite matrices) our cost expressed in terms of size of matrix
    $A$ will be:
    \begin{equation}
    \label{eq:fact_lu_cost_a}
	    f_{\mathrm{lu}}(a) = \frac{2}{3}a^3 - \frac{1}{2}a^2 + \frac{5}{6}a
    \end{equation}

\subsection{Backward and forward substitution}
    In order to compute: $A^{-1}B$ we need only to use factors of matrix A, that
    is, for LU factorization we will compute:
    \[
        A^{-1}B = U^{-1}L^{-1}B
    \]
    and for Cholesky decomposition:
    \[
        A^{-1}B = \left(L^{T}\right)^{-1}L^{-1}B
    \]
    The cost for LU factorization is
    \[
	    f(n) = \mathrm{RHS} (n^2 - n)
    \]
    What directly leads to complexity of computing $A^{-1}B$ expressed in sizes
    of matrices $A$ and $B$:
    \begin{equation}
    \label{eq:bfs}
            f_{\mathrm{bfs|lu}}(a, b) = b(2a^2 - a)
    \end{equation}

    And for Cholesky factorization we have similar equation:
    \begin{equation}
	    f_{\mathrm{bfs|chol}}(a, b) = 2ba^2
    \end{equation}

\subsection{Matrix-by-matrix multiplication}
    Number of floating-point operations for multiplication of two matrices of
    well-known sizes is trivial, assuming
    $C_{b \times a}$ and $(A^{-1}B)_{a \times b}$ we have:
    \begin{equation}
    \label{eq:mult}
        f_{\mathrm{mult}}(a, b) = 2ab^2
    \end{equation}

\subsection{Substraction step}
    Last stage is substraction step that in case of two matrices of size
    $b \times b$ the total number of FLOPS is:
    \begin{equation}
        f_{\mathrm{sub}}(b) = b^2
    \end{equation}

\subsection{Merging step}
    Transferring data towards root of the partitioning tree requires additional
    $b^2$ additions. We can take them also into consideration:
    \begin{equation}
        f_{\mathrm{merge}}(b) = b^2
    \end{equation}

\subsection{Estimation of total number of FLOPS in partitioning tree node}
    Finally we can sum up all of the above complexities to get total number of
    FLOPS for LU (\ref{eq:cost_lu_total}):
    \begin{align}
    \label{eq:cost_lu_total}
            f(a, b) = & f_{\mathrm{lu}}(a) + f_{\mathrm{bfs|lu}}(a, b) + 
            f_{\mathrm{mult}}(a, b) + f_{\mathrm{sub}}(b) +
            f_{\mathrm{merge}}(b) \nonumber \\
	    = & \frac{2}{3}a^3 - \frac{1}{2}a^2 + \frac{5}{6}a + b(2a^2 - a) +
	    2ab^2 + 2b^2
    \end{align}
    Similarly, for Cholesky decomposition, the total number of FLOPS per matrix
    is given by the following equation (\ref{eq:cost_cholesky_total}):
    \begin{align}
    \label{eq:cost_cholesky_total}
            f(a, b) = & f_{chol}(a) + f_{bfs|chol}(a, b) + f_{mult}(a, b) + f_{sub}(b) +
            f_{trans}(b) \nonumber \\
	    = & \frac{1}{3}a^3 + \frac{1}{2}a^2 + \frac{1}{6}a + 2ba^2 +
	    2ab^2 + 2b^2
    \end{align}
\end{section}

\begin{section}{FLOPS distribution}
    Computing number of FLOPS is trivial when comparing to the problem of
    assigning FLOPS to unknowns. Important goal is to distribute FLOPS as
    equally as possible. Equality means that every operation performed on
    submatrix should modify unknowns associated with both rows and columns of
    given block. This rule will be later referenced as ''rule of equality''.

    Local system of linear equations is constructed as follows:
    \[
            \begin{bmatrix}
                    A_{a \times a} & B_{a \times b} \\
                    C_{b \times a} & D_{b \times b}
            \end{bmatrix}
            \begin{bmatrix}
                    \phi_{1} \\
                    \phi_{2} \\
                    \vdots \\
                    \phi_{a} \\
                    \phi_{a+1} \\
                    \vdots \\
                    \phi_{a + b - 1} \\
                    \phi_{a + b}
            \end{bmatrix} =
            \overline{b}
    \]

    Please notice, that in this notation uses local enumeration of DOFs.
    Estimator traces mappings between local and global enumerations and fill
    flops array accordingly.

    Assuming LU factorization, the first $a$ variables will be totally
    eliminated and number of flops mapped to any of them is given by
    equation (\ref{eq:fact_summand}):
    \begin{equation}
    \label{eq:fact_summand}
	    f_{\mathrm{fact}(\phi_i)}(a) = \frac{2}{3}a^3 - \frac{1}{2}a^2 +
	    \frac{5}{6}a
    \end{equation}
    For $i \in \{1, \dots, a\}$. Equation (\ref{eq:fact_summand}) denotes cost
    of factorization related to unknown $\phi_i$.


    This is implication of equation (\ref{eq:fact_lu_cost_a}) divided by number
    of unknowns ($a$).
    Similarly, we can derive equation for Cholesky factorization.

    This approach will give us an fair division of number of flops
    between all of unknowns involved in factorization step.

    Situation becomes more complicated in terms of backward and foward
    substitution step presented in equation (\ref{eq:bfs}) and computing
    $CA^{-1}B$ (\ref{eq:mult}). Estimator should divide number of steps equally
    between all of the degrees of freedom. Equation (\ref{eq:bfs_mult_summand})
    shows mapping between flops and unknowns, trying to follow ''near-equal''
    rule:
    \begin{equation}
    \label{eq:bfs_mult_summand}
    \begin{split}
	    f_{\mathrm{bfs|lu}}(a, b) + f_{\mathrm{mult}}(a, b) = ab(2a-1) + 2ab^2 \\
            = \underbrace{2b(ab)}_{\textrm{For DOFs related to Schur complement}}
	    + \underbrace{ab(2a-1)}_{\textrm{For DOFs that will be fully eliminated}}
    \end{split}
    \end{equation}

    Computing $D = D - CA^{-1}B$ is simple and affects only degrees of freedom
    belonging to Schur complement.

    Hence:
    \begin{equation}
            f_{\mathrm{total}(\phi_i)} = \left \{
                \begin{array}{l l}
			\frac{4a^2 - 3a + 5}{6} + b(2a-1) & \mathrm{for}\; i \in \{1, \dots, a\} \\
                        (2a + 2)b & \mathrm{for}\; i \in \{a + 1, \dots, a + b\}
                \end{array}
                \right .
    \end{equation}
\end{section}
\end{document}
