#include "Node.hpp"
#include <set>
#include <algorithm>
#include "Analysis.hpp"

void Node::setLeft(Node *left)
{
	this->left = left;
}

void Node::setRight(Node *right)
{
	this->right = right;
}

void Node::setParent(Node *parent)
{
	this->parent = parent;
}

void Node::addElement(Element *e)
{
	this->mergedElements.push_back(e);
}

void Node::clearElements(){
	this->mergedElements.clear();
}

Node *Node::getLeft() const
{
	return this->left;
}

Node *Node::getRight() const
{
	return this->right;
}

Node *Node::getParent() const
{
	return this->parent;
}

std::vector<Element *> &Node::getElements()
{
	return this->mergedElements;
}

int Node::getId() const
{
	return this->node;
}

void Node::setId(int n)
{
	this->node = n;
}

void Node::addSupernode(uint64_t supernode)
{
	this->supernodes.push_back(supernode);
}

std::vector<uint64_t> &Node::getSupernodes()
{
	return this->supernodes;
}

void Node::clearSupernodes(){
	this->supernodes.clear();
}

void Node::setSupernodesToElim(uint64_t supernodes)
{
	this->supernodesToElim = supernodes;
}

uint64_t Node::getSupernodesToElim() const
{
	return this->supernodesToElim;
}

void Node::computeOffsets()
{
	uint64_t j;
	uint64_t k;

	j = 0;
	k = 0;
	offsets.resize(getSupernodes().size()+1);
	for (uint64_t i : getSupernodes()) {
		offsets[k] = j;
		j += (*meshSupernodes)[i];
		++k;
	}
	offsets[k] = j;
}



/* DEBUG*/

int Node::treeSize(){
	int ret = 1;
	if (this->getLeft() != NULL){
		ret += this->getLeft()->treeSize();
	}
	if (this->getRight() != NULL){
		ret += this->getRight()->treeSize();
	}
	return ret;
}


unsigned long Node::getSizeInMemory(bool recursive)
{
	unsigned long total = this->supernodes.size()*this->supernodes.size()*sizeof(double) + this->supernodes.size()*sizeof(double*);
	if (recursive && left != NULL && right != NULL) {
		total += left->getSizeInMemory() + right->getSizeInMemory();
	}
	return total;
}

unsigned long Node::getFLOPs(unsigned int a, unsigned int b)
{
	//fprintf(stderr, "Node: %d\n", this->getId());
	//fprintf(stderr, "\tsupernodes: %d\n", this->supernodes.size());
	//fprintf(stderr, "\tto elim: %d\n", this->supernodesToElim);
	return a * (6 * b * b - 6 * a * b + 6 * b + 2 * a * a - 3 * a + 1) / 6;
}

unsigned long Node::getMemoryRearrangements()
{
	unsigned long total = 0;

	if (left != NULL && right != NULL) {
		unsigned long memLeft = (left->getSupernodes().size() - left->getSupernodesToElim());
		memLeft *= memLeft;
		unsigned long memRight = (right->getSupernodes().size() - right->getSupernodesToElim());
		memRight *= memRight;

		total = memLeft+memRight+left->getMemoryRearrangements()+right->getMemoryRearrangements();
	}

	return total;
}


/*END OF DEBUG*/
