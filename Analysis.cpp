#include "Analysis.hpp"
#include <algorithm>

using namespace std;

void Analysis::estimateElimCosts(Node *node)
{
	uint64_t a, b, i, totalDofs;

	totalDofs = 0;
    a = 0;
    b = 0;
	/*
	 * Array node->meshSupernodes contains number of degrees of freedom
	 * per supernode.
	 *
	 * node->getSupernodes().size() - number of super nodes.
	 *
	 * totalDofs = total number of degrees of freedom.
	 */
	for (i = 0; i < node->getSupernodes().size(); ++i) {
        totalDofs += (*node->meshSupernodes)[i];
        if (i < node->getSupernodesToElim())
                a += (*node->meshSupernodes)[i];
        else
                b += (*node->meshSupernodes)[i];
    }

	/*
	 * The condition below should be true in nearly all cases. However,
	 * in some cases there may be zero rows to eliminate. Lets protect
	 * against such case.
	 */
	if (node->getSupernodesToElim() > 0) {
		/* For each supernode in matrix. */
		for (i = 0; i < node->getSupernodes().size(); ++i) {
			if (i < node->getSupernodesToElim()) {
				/* If given supernode is eliminated, add to its
				 * total flops number value:
				 * 2 * (totalDofs - 1)
				 */
				(*node->meshFlops)[node->getSupernodes()[i]] +=
				    (2 * a * a - 1) / 3 + b * (a + 1) / 2;
			} else {
				/*
				 * Else: do not include computing scaling &
				 * inversion.
				 */
				(*node->meshFlops)[node->getSupernodes()[i]] +=
				    a * b + 2 * b;
			}

		}
	}
	/*
	 * Do the same for the children.
	 */
	if (node->getLeft() && node->getRight()) {
		Analysis::estimateElimCosts(node->getLeft());
		Analysis::estimateElimCosts(node->getRight());
	}
}

void Analysis::nodeAnaliser(Node *node, set<uint64_t> *parent)
{
	auto getAllSupernodes = [] (Node *n) {
		set<uint64_t> *supernodes = new set<uint64_t>();
		for (Element *e : n->getElements()) {
			for (uint64_t supernode : e->supernodes)
				supernodes->insert(supernode);
		}
		return supernodes;
	};

	set<uint64_t> *common;

	if (node->getLeft() != NULL && node->getRight() != NULL) {
		set<uint64_t> *lSupernodes = getAllSupernodes(node->getLeft());
		set<uint64_t> *rSupernodes = getAllSupernodes(node->getRight());

		common = new set<uint64_t>;
		std::set_intersection(lSupernodes->begin(), lSupernodes->end(),
				      rSupernodes->begin(), rSupernodes->end(),
				      std::inserter(*common, common->begin()));


		for (auto p = parent->cbegin(); p != parent->cend(); ++p) {
			if (lSupernodes->count(*p) || rSupernodes->count(*p))
				common->insert(*p);
		}

		delete (lSupernodes);
		delete (rSupernodes);


		Analysis::nodeAnaliser(node->getLeft(), common);
		Analysis::nodeAnaliser(node->getRight(), common);

	} else {
		common = getAllSupernodes(node);
	}


	int i = 0;

	for (uint64_t supernode : *common) {
		if (!parent->count(supernode)) {
			node->addSupernode(supernode);
			++i;
		}
	}

	node->setSupernodesToElim(i);

	for (uint64_t supernode : *common) {
		if (parent->count(supernode)) {
			node->addSupernode(supernode);
		}
	}
#ifdef DEBUG
	for (uint64_t cnode: node->getSupernodes()) {
		fprintf(stderr, "Tree node: %lu | dof: %lu\n", node->getId(),
			cnode+1);
	}
#endif
	delete common;
}

void Analysis::doAnalise(Mesh *mesh)
{
	Node *root = mesh->getRootNode();
	std::set<uint64_t> *parent = new set<uint64_t>();
	Analysis::nodeAnaliser(root, parent);
	Analysis::mergeAnaliser(root);
	delete parent;
}

void Analysis::mergeAnaliser(Node *node)
{
	Node *left;
	Node *right;
	int i;

	left = node->getLeft();
	right = node->getRight();

	if (left != NULL && right != NULL) {
		node->leftPlaces.resize(left->getSupernodes().size() - left->getSupernodesToElim());
		node->rightPlaces.resize(right->getSupernodes().size() - right->getSupernodesToElim());

		map<uint64_t, uint64_t> reverseMap;

		for (i=0; i<node->getSupernodes().size(); ++i) {
			reverseMap[node->getSupernodes()[i]] = i;
		}

		for (i=left->getSupernodesToElim(); i<left->getSupernodes().size(); ++i) {
			node->leftPlaces[i-left->getSupernodesToElim()] = reverseMap[left->getSupernodes()[i]];
		}

		for (i=right->getSupernodesToElim(); i<right->getSupernodes().size(); ++i) {
			node->rightPlaces[i-right->getSupernodesToElim()] = reverseMap[right->getSupernodes()[i]];
		}

		Analysis::mergeAnaliser(left);
		Analysis::mergeAnaliser(right);
	}
	node->computeOffsets();
}


