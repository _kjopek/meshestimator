CC		= cc
CPP		= c++
CFLAGS		= -O3 -I/usr/local/include -g
CPPFLAGS	= -O3 -I/usr/local/include -std=c++11 -g
LDFLAGS		= -L/usr/local/lib
OBJS		= Analysis.o Mesh.o Node.o analyser.o
DEPS		= Analysis.h Element.h Mesh.h Node.h


analiser: $(OBJS)
	@echo "Linking analyser."
	@$(CPP) $(CPPFLAGS) $(LDFLAGS) $(OBJS) -o analyser

.c.o: $(DEPS)
	@echo "Compiling: $<"
	@$(CC) $< -c $(CFLAGS) -o $@

.cpp.o: $(DEPS)
	@echo "Compiling: $<"
	@$(CPP) $< -c $(CPPFLAGS) -o $@

.PHONY:	clean

clean:
	@echo "Cleaning"
	@rm -f $(OBJS) analyser
